Antidote a command line mail user agent
=======================================
About
-----
Nothing to see here right now ;)

Antidote is my vision of a command line mua (mail user agent) written in vala.

For now this code is **very experimental and incomplete** but later it will become
a full-fledged command line mua.

Bugreports, patches and suggestions are welcome!

Have fun!
Bernhard

Build
-----
    meson build
    cd build
    ninja