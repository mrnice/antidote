namespace Antidote {
	class Logger {
		public Logger () {}

		public enum LogLevel {
			LOW,
			INFO,
			DEBUG,
			GREEN;

			public string to_string() {
				switch (this) {
					case LOW:
						return "LOW";
					case INFO:
						return "INFO";
					case DEBUG:
						return "DEBUG";
					case GREEN:
						return "GREEN";
					default:
						assert_not_reached();
				}
			}
		}

		public LogLevel log_level { get; set; default=LogLevel.INFO;}
		public string name {get; set; default="Antidote";}

		public void log(LogLevel level, string message) {
			if(level <= log_level)
				print("["+level.to_string()+":"+name+"] " + message);
		}
	}
}
