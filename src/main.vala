/* FIXME: This file is RAD and needs a lot of refactoring ;) */
using Antidote;
using Gee;
using Curses;


/* message token */
struct Token {
	public string intending;
	public string token;
}

//destructive
string get_intending(LinkedList<unichar> stack) {
	unichar n;
	string intending ="";
			bool last_level = false;
			LinkedList<unichar> s = new LinkedList<unichar>();
			while ((n=stack.poll_head())!=(unichar)null) {
				s.offer_head(n);
				if(n==' ') {
					if(!last_level) {
						last_level=true;
						intending="\\" + intending;
					}
					else
						intending="|-" + intending;
				}
				if(n=='(') {
					if(stack.peek_head()!=(unichar)null)
						intending="-" + intending;
				}
			}
			while ((n=s.poll_head())!=(unichar)null)
				stack.offer_head(n);
	return intending;
}

ArrayList<Token?> parse_thread(string th) {
	string thread = th.replace("* THREAD ","");
	ArrayList<Token?> list = new ArrayList<Token?> ();
	unichar c;
	int index=0;
	LinkedList<unichar> stack = new LinkedList<unichar>();
	string token = "";
	Token token_struct = Token();

	while (thread.get_next_char(ref index, out c)) {
		if (c == '(' ) {
			stack.offer_head('(');
		}
		else if (c == ')') {
			if (token!="") {
				token_struct.intending = get_intending(stack)+"-";
				token_struct.token = token;
				list.add(token_struct);
			}
				//print(get_intending(stack) + "-" + token + "\n");
				//remove one level
				while(stack.poll_head()==' ');
				token="";
		}
		else if (c == ' ') {
			if (token!="") {
				token_struct.intending = get_intending(stack)+"-";
				token_struct.token = token;
				list.add(token_struct);
				//print(get_intending(stack) + "-" + token + "\n");
				//add one level
				stack.offer_head(' ');
				token="";
			}
		}
		else { //FIXME: check chars for non valid items
			token = token + c.to_string();
		}
	}
	return list;
}

void init_curses() {
	initscr ();
	noecho();
	cbreak();
	stdscr.keypad(true);
	start_color ();
	init_pair(1, Color.GREEN, Color.BLUE);
	init_pair(2, Color.YELLOW, Color.BLACK);
	init_pair(3, Color.WHITE, Color.BLUE);
	bkgd(COLOR_PAIR(2));
	refresh();
}

void draw_menu(Window win) {
	win.clear();
	win.bkgd(COLOR_PAIR(2));
	win.box(0,0);
	win.attron(Attribute.BOLD);
	win.mvaddstr(1, 2, "F1:");
	win.attroff(Attribute.BOLD);
	win.mvaddstr(1, 6, "messages");
	win.refresh();
}

void draw_folders(Window win) {
	win.clear();
	win.bkgd(COLOR_PAIR(2));
	win.box(0,0);
	win.attron(Attribute.BOLD);
	win.mvaddstr(1, 2, "F1:");
	win.attroff(Attribute.BOLD);
	win.mvaddstr(1, 6, "messages");
	win.refresh();
}

void draw_header(Window win) {
	win.clear();
	win.bkgd(COLOR_PAIR(2));
	win.attron(Attribute.BOLD);
	win.mvaddstr(0, 1, "Antidote version 0.1");
	win.refresh();
}

void draw_threads(Window win,ArrayList<Token?> list,IMAP imap,int start_line) {
	win.clear();
	win.bkgd(COLOR_PAIR(2));
	win.box(0,0);

	//FIME:out of bounds
	for (int i = start_line; i<start_line+LINES-9;i++) {
		var t = list.get(i);
		if(t!=null) {
			//FIXME: only fetch the header if not already available!
			var msgs = imap.get_headers(t.token,t.token);
			Message msg;
			msg = msgs.get(t.token.to_int());
			win.mvaddstr(i-start_line, 1, t.token);
			win.mvaddstr(i-start_line, 5, msg.date);
			win.mvaddstr(i-start_line, 15, "\t"+msg.from);
			win.mvaddstr(i-start_line, 30, "\t"+t.intending+msg.subject);
		}
	}

	win.refresh();
}

//void print_list(ArrayList<Token?> list,IMAP imap) {
//	//print the list
//	print(list.size.to_string());
//	for (int i = 300; i<list.size;i++) {
//		print("INDEX: %i\n",i);
//	//foreach (Token? t in list) {
//		var t = list.get(i);
//		if(t!=null) {
//			//print("CURRENTTOKEN: %s",t.token);
//			var msgs = imap.get_headers(t.token,t.token);
//			Message msg;
//			msg = msgs.get(t.token.to_int());
//	//		print(@"$(t.token)\t$(t.intending) $(msg.date) \t$(msg.from) \t$(msg.subject)\n");
//			print(@"$(t.token)\t$(t.intending)$(msg.subject)\n");
//				//print(@"$(delim) $(t.token)\n");
//		}
//	}
//}

int main (string[] args) {
int current_line=1;

	init_curses();
	var header_win = new Window (1, COLS, 0, 0);
	var menu_win = new Window (6, COLS, LINES-6, 0);
	var threads_win = new Window (LINES-7, COLS-30, 1, 30);
	var folders_win = new Window (LINES-7, 30, 1, 0);

	draw_header(header_win);
	draw_menu(menu_win);
	draw_folders(folders_win);



	try {
		var imap = new IMAP();
		KeyFile conf_file = new GLib.KeyFile();
		conf_file.load_from_file(Environment.get_home_dir()+"/.config/antidote/antidote.conf", GLib.KeyFileFlags.NONE);
		//FIXME: support lists later! just one account for now
		var accounts = conf_file.get_string("general", "accounts");
		imap.host = conf_file.get_string(accounts, "imap_host");
		imap.port = (uint16) conf_file.get_integer(accounts, "imap_port");
		imap.username = conf_file.get_string(accounts, "imap_username");
		//FIXME: support keyring later!
		imap.password = conf_file.get_string(accounts, "imap_password");
		imap.ssl = conf_file.get_boolean(accounts, "imap_ssl");
		imap.connect();
		imap.login();
		//imap.list("*");
		imap.examine("INBOX");

		var thread = imap.thread("REFERENCES", "UTF-8", "ALL");
		//var thread = imap.thread("ORDEREDSUBJECT", "UTF-8", "ALL");
		//print (thread);
		var list = parse_thread(thread);

		draw_threads(threads_win,list,imap,current_line);
	while(true) {
		stdscr.getch ();
current_line=current_line+LINES-9;
		draw_threads(threads_win,list,imap,current_line);

	}

	} catch (Error e) {
		stderr.printf ("%s\n", e.message);
	}


	endwin ();

	return 0;
}
